package com.example.suma;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    public void process(View v) {
    	
    	System.out.println("hola");
    	Log.d("hola", "hola");
    	
    	EditText number1EditText = (EditText)findViewById(R.id.editText1);
    	EditText number2EditText = (EditText)findViewById(R.id.editText2);
    	
    	float n1 = Float.parseFloat(number1EditText.getText().toString());
    	float n2 = Float.parseFloat(number2EditText.getText().toString());
    	
    	Context context = getApplicationContext();
    	CharSequence text = String.valueOf(n1 + n2);
    	int duration = Toast.LENGTH_SHORT;

    	Toast toast = Toast.makeText(context, text, duration);
    	toast.show();
    	
    }
}
